package planning_algorithms;

import java.util.ArrayList;
import java.util.Collections;

import planning_algorithms.exceptions.ImpossibleToExecuteException;
import process.Process;

public class SJFWithExpropriation extends PlanningAlgorithm {

    private int timeWorking = 0;

    public SJFWithExpropriation(ArrayList<Process> processesQueue) {
        super(processesQueue);
    }

    @Override
    public double simulate() {

        ArrayList<Process> allProcessesQueue = super.getProcessesQueue();
        ArrayList<Process> availableProcessesQueue = new ArrayList<>();

        //checks whether simulation can be carried out with such data provided
        if (isDataIncorrect(allProcessesQueue)) {
            throw new ImpossibleToExecuteException();
        }

        //sort all processes by their time of arrival
        Collections.sort(allProcessesQueue, Process::compareByArrival);

        while (!allProcessesQueue.isEmpty()) {

            //clone all processes that arrived within last execution iteration
            // (at start all processes arriving at 0 are cloned)
            allProcessesQueue.forEach(process -> {
                if (wasProcessAddedWithinLastCycle(process, timeWorking - 1, timeWorking)) {
                    availableProcessesQueue.add(process);
                }
            });

            //when all processes haven't been finished yet, but there are currently no processes to execute
            //we skip to fastest arriving process
            if (availableProcessesQueue.isEmpty()) {
                timeWorking = allProcessesQueue.get(0).getTimeOfArrival();
            } else {

                //sort available processes by how long will they take, accordingly to sjf premise
                Collections.sort(availableProcessesQueue, Process::compareByRemainingCpuTime);

                //get quickest to finish process available
                Process currentProcess = availableProcessesQueue.get(0);

                //decrement processes needed time to finish its job
                currentProcess.setRemainingTimeOfCpuUsage(currentProcess.getRemainingTimeOfCpuUsage() - 1);

                //increment cpu's working time
                timeWorking++;

                //set for how long process had to wait so far
                currentProcess.setTimeWaiting(calcProcessesTimeWaiting(currentProcess));

                //if process doesn't need any more time, meaning he has ended
                if (currentProcess.getRemainingTimeOfCpuUsage() == 0) {

                    //add processes time waiting to sum of all processes waiting times
                    super.addToWaitingTimeSum(currentProcess.getTimeWaiting());

                    //increment number of processes finished
                    super.processFinished();

                    //remove him from both queues since he has been finished
                    availableProcessesQueue.remove(currentProcess);
                    allProcessesQueue.remove(currentProcess);

                }

            }

        }


        return super.getAverageWaitingTime();
    }

    //processes waiting time is time for how long he HASN'T been executed since he arrived
    private int calcProcessesTimeWaiting(Process process) {
        return timeWorking - process.getTimeOfArrival() - (process.getTimeOfCpuUsage() - process.getRemainingTimeOfCpuUsage());
    }

    //if any of this is true, incorrect data has been provided
    private boolean isDataIncorrect(ArrayList<Process> queue) {
        return (queue == null || queue.isEmpty());
    }

}
