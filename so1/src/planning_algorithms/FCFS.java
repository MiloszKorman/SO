package planning_algorithms;

import java.util.ArrayList;
import java.util.Collections;

import planning_algorithms.exceptions.ImpossibleToExecuteException;
import process.Process;

public class FCFS extends PlanningAlgorithm {

    private int timeWorking = 0;

    public FCFS(ArrayList<Process> processesQueue) {
        super(processesQueue);
    }

    //no need to simulate process arrival to queue since it's sorted by time of arrival
    //and processes are executed respectively
    @Override
    public double simulate() {

        ArrayList<Process> processesQueue = super.getProcessesQueue();

        //checks whether simulation can be carried out with such data provided
        if (isDataIncorrect(processesQueue)) {
            throw new ImpossibleToExecuteException();
        }

        //sort processes by their time of arrival
        Collections.sort(processesQueue, Process::compareByArrival);

        while (!processesQueue.isEmpty()) { //as long as there are processes left to execute

            //in case oldest process hasn't come yet, skip to when he arrives
            if (timeWorking < processesQueue.get(0).getTimeOfArrival()) {
                timeWorking = processesQueue.get(0).getTimeOfArrival();
            }

            //retrieve oldest process in queue
            Process currentProcess = processesQueue.get(0);

            //time he waited is equals to time for how long processor has been working
            // from his arrival to start of his execution
            currentProcess.setTimeWaiting(timeWorking - currentProcess.getTimeOfArrival());

            //add his waiting time to sum of all processes waiting times
            super.addToWaitingTimeSum(currentProcess.getTimeWaiting());

            //execute this process, meaning add his time of Cpu usage to processors time working
            timeWorking += currentProcess.getTimeOfCpuUsage();

            //increment number of processes finished
            super.processFinished();

            //remove process from queue since he has been executed
            processesQueue.remove(currentProcess);
        }

        return super.getAverageWaitingTime();
    }

    //if any of this is true, incorrect data has been provided
    private boolean isDataIncorrect(ArrayList<Process> queue) {
        return (queue == null || queue.isEmpty());
    }

}
