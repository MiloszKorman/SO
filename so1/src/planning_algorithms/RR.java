package planning_algorithms;

import java.util.ArrayList;
import java.util.Collections;

import planning_algorithms.exceptions.ImpossibleToExecuteException;
import process.Process;

public class RR extends PlanningAlgorithm {

    private int quantityOfTime;

    private int timeWorking = 0;
    private int timeWorkingStartOfCycle = -1;

    private Process unfinishedProcess = null;

    public RR(ArrayList<Process> processesQueue, int quantityOfTime) {
        super(processesQueue);
        this.quantityOfTime = quantityOfTime;
    }

    @Override
    public double simulate() {

        ArrayList<Process> allProcessesQueue = super.getProcessesQueue();
        ArrayList<Process> availableProcessesQueue = new ArrayList<>();

        //checks whether simulation can be carried out with such data provided
        if (isDataIncorrect(allProcessesQueue)) {
            throw new ImpossibleToExecuteException();
        }

        Collections.sort(allProcessesQueue, Process::compareByArrival); //all processes are sorted by arrive time

        while (!allProcessesQueue.isEmpty()) { //as long as there are processes left to execute

            //clone all processes that arrived within last execution cycle
            // (at start all processes arriving at 0 are cloned)
            allProcessesQueue.forEach(process -> {
                if (wasProcessAddedWithinLastCycle(process, timeWorkingStartOfCycle, timeWorking)) {
                    availableProcessesQueue.add(process);
                }
            });

            //if in last cycle process hasn't been finished, add him at the end of queue for execution
            if (unfinishedProcess != null) {
                availableProcessesQueue.add(unfinishedProcess);
                unfinishedProcess = null;
            }

            //execution cycle is marked by start of executing certain process and end
            //timeWorkingStartOfCycle stores time of start of a cycle,
            // which is timeWorking when processes execution begins
            timeWorkingStartOfCycle = timeWorking;

            //when all processes haven't been finished yet, but there are currently no processes to execute
            //we skip to fastest arriving process
            if (availableProcessesQueue.isEmpty()) {
                timeWorking = allProcessesQueue.get(0).getTimeOfArrival();
            } else {

                //get process on the bottom of available processes queue
                Process currentProcess = availableProcessesQueue.get(0);

                if (currentProcess.getRemainingTimeOfCpuUsage() <= quantityOfTime) { //for processes that
                                                                                    //will end within this cycle

                    //process will be executed for as long as he wants, since it's a value smaller
                    //than quantity of time
                    timeWorking += currentProcess.getRemainingTimeOfCpuUsage();

                    //he has been finished, so there doesn't need processors time any longer
                    currentProcess.setRemainingTimeOfCpuUsage(0);

                    //calculate and set for how long he had to wait in this simulation
                    currentProcess.setTimeWaiting(calcProcessesTimeWaiting(currentProcess));

                    //add his wait time to sum of all processes waiting times
                    super.addToWaitingTimeSum(currentProcess.getTimeWaiting());

                    //increment number of processes finished
                    super.processFinished();

                    //remove him from both queues since he has been finished
                    availableProcessesQueue.remove(currentProcess);
                    allProcessesQueue.remove(currentProcess);

                } else { //for processes that will have to be continued in the future

                    //process is executed for a whole provided quantity of time
                    timeWorking += quantityOfTime;

                    //decrease his remaining time of cpu usage by the provided quantity of time
                    currentProcess.setRemainingTimeOfCpuUsage(
                            currentProcess.getRemainingTimeOfCpuUsage() - quantityOfTime
                    );

                    //calculate and set for how long he had to wait so far
                    currentProcess.setTimeWaiting(calcProcessesTimeWaiting(currentProcess));

                    //remove him from short queue of available processes and mark him as unfinished
                    //so that he can be added at the end at the start of next cycle
                    availableProcessesQueue.remove(currentProcess);
                    unfinishedProcess = currentProcess;

                }

            }

        }

        return super.getAverageWaitingTime();
    }

    //processes waiting time is time for how long he HASN'T been executed since he arrived
    private int calcProcessesTimeWaiting(Process process) {
        return timeWorking - process.getTimeOfArrival() - (process.getTimeOfCpuUsage() - process.getRemainingTimeOfCpuUsage());
    }

    //if any of this is true, incorrect data has been provided
    private boolean isDataIncorrect(ArrayList<Process> queue) {
        return (quantityOfTime < 1 || queue == null || queue.isEmpty());
    }

    public int getQuantityOfTime() {
        return quantityOfTime;
    }

}
