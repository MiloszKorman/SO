package planning_algorithms;

import planning_algorithms.exceptions.ImpossibleToExecuteException;
import process.Process;

import java.util.ArrayList;

public abstract class PlanningAlgorithm {

    private long waitingTimeSum = 0;
    private int numberOfProcessesFinished = 0;
    private ArrayList<Process> processesQueue;

    PlanningAlgorithm(ArrayList<Process> processesQueue) {
        this.processesQueue = processesQueue;
    }

    ArrayList<Process> getProcessesQueue() {
        return processesQueue;
    }

    //is calculated by dividing sum of all processes waiting times and number of processes finished
    public double getAverageWaitingTime() {
        if (numberOfProcessesFinished == 0) {
            return -1;
        } else {
            return ((double) waitingTimeSum) / numberOfProcessesFinished;
        }
    }

    //increment number of processes finished if one has just been finished
    void processFinished() {
        numberOfProcessesFinished++;
    }

    public abstract double simulate() throws ImpossibleToExecuteException;


    void addToWaitingTimeSum(int processesWaitTime) {
        waitingTimeSum += processesWaitTime;
    }

    //checks if process arrived within last cycle
    boolean wasProcessAddedWithinLastCycle(Process process, int startOfCycle, int endOfCycle) {
        return (startOfCycle < process.getTimeOfArrival() && process.getTimeOfArrival() <= endOfCycle);
    }

}
