package planning_algorithms.exceptions;

//thrown when simulation can't be carried out because of incorrect provided data
public class ImpossibleToExecuteException extends RuntimeException {
    public ImpossibleToExecuteException() {
        super("It's impossible to simulate this algorithm with provided data");
    }
}
