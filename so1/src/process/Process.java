package process;

public class Process {

    private static int numberOfProcesses = 0;

    private int id;
    private int timeOfCpuUsage;
    private int remainingTimeOfCpuUsage;
    private int timeOfArrival = 0;
    private int timeWaiting = 0;

    //assigns id for newly created processes, based on how many have been created
    //up till this point
    private static int assignId() {
        return ++numberOfProcesses;
    }

    public Process(int timeOfCpuUsage, int timeOfArrival) {
        id = assignId();
        this.timeOfCpuUsage = timeOfCpuUsage;
        this.remainingTimeOfCpuUsage = timeOfCpuUsage;
        this.timeOfArrival = timeOfArrival;
    }

    public int getId() {
        return id;
    }

    public int getTimeOfCpuUsage() {
        return timeOfCpuUsage;
    }

    public int getRemainingTimeOfCpuUsage() {
        return remainingTimeOfCpuUsage;
    }

    public int getTimeOfArrival() {
        return timeOfArrival;
    }

    public int getTimeWaiting() {
        return timeWaiting;
    }

    public void setRemainingTimeOfCpuUsage(int remainingTimeOfCpuUsage) {

        this.remainingTimeOfCpuUsage = remainingTimeOfCpuUsage;
    }

    public void setTimeWaiting(int timeWaiting) {
        this.timeWaiting = timeWaiting;
    }

    public void reset() {
        this.remainingTimeOfCpuUsage = timeOfCpuUsage;
    }

    //compares processes by which came first
    public static int compareByArrival(Process process1, Process process2) {
        return Integer.compare(process1.timeOfArrival, process2.timeOfArrival);
    }

    //compares processes by which is to finish first
    public static int compareByRemainingCpuTime(Process process1, Process process2) {
        return Integer.compare(process1.remainingTimeOfCpuUsage, process2.remainingTimeOfCpuUsage);
    }

}
