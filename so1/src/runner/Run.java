package runner;

import planning_algorithms.*;

import java.util.ArrayList;
import java.util.Scanner;

import process.Process;


public class Run {

    private static int nOfProcesses = 0;
    private static int maxCpuTime = 0;
    private static int maxArrivalTime = 0;
    private static int quantityOfTime = 0;
    private static float FCFSSimulationsSum = 0;
    private static float SJFWIthSimulationsSum = 0;
    private static float SJFWithoutSimulationsSum = 0;
    private static float RRSimulationsSum = 0;
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("How accurate do you want results to be?: ");
        int accuracy = getIntBetween(1, 100);

        createData();

        System.out.println(nOfProcesses + " processes in queue.");
        System.out.println("Max time of cpu usage: " + maxCpuTime);
        System.out.println("Max time of arrival: " + maxArrivalTime);

        for (int i = 0; i < accuracy; i++) {

            ArrayList<Process> processesQueue = generate(nOfProcesses, maxCpuTime, maxArrivalTime);

            ArrayList<PlanningAlgorithm> algorithms = new ArrayList<>();

            algorithms.add(new FCFS(new ArrayList<>(processesQueue)));
            algorithms.add(new SJFWithoutExpropriation(new ArrayList<>(processesQueue)));
            algorithms.add(new SJFWithExpropriation(new ArrayList<>(processesQueue)));
            algorithms.add(new RR(new ArrayList<>(processesQueue), quantityOfTime));

            System.out.println();

            for (PlanningAlgorithm algorithm : algorithms) {
                algorithm.simulate();
                System.out.print(
                        algorithm.getClass().getSimpleName() +
                                ", average waiting time: " +
                                String.format("%.2f", algorithm.getAverageWaitingTime())
                );

                if (algorithm instanceof RR) {
                    System.out.print(" Quantity of time: " + ((RR) algorithm).getQuantityOfTime());
                }

                if(algorithm instanceof FCFS) {
                    FCFSSimulationsSum += algorithm.getAverageWaitingTime();
                } else if(algorithm instanceof SJFWithoutExpropriation) {
                    SJFWithoutSimulationsSum += algorithm.getAverageWaitingTime();
                } else if(algorithm instanceof SJFWithExpropriation) {
                    SJFWIthSimulationsSum += algorithm.getAverageWaitingTime();
                } else if(algorithm instanceof RR) {
                    RRSimulationsSum += algorithm.getAverageWaitingTime();
                } else {
                    System.out.println("Something went wrong");
                }

                System.out.println();
                processesQueue.forEach(Process::reset);
            }

        }

        sc.close();

        System.out.println("\n\n");
        System.out.println("Fcfs average time in " + accuracy + " runs: " + String.format("%.2f", FCFSSimulationsSum/accuracy));
        System.out.println("SJF without expropriation average time in " + accuracy + " runs: " + String.format("%.2f", SJFWithoutSimulationsSum/accuracy));
        System.out.println("SJF with expropriation average time in " + accuracy + " runs: " + String.format("%.2f", SJFWIthSimulationsSum/accuracy));
        System.out.println("RR average time in " + accuracy + " runs: " + String.format("%.2f", RRSimulationsSum/accuracy) + " Quantity of time: " + quantityOfTime);

    }

    //user decides whether he wants to provide the queue or generate random one
    private static void createData() {

        System.out.print("Do you want to provide data (y/n)?: ");

        if (getTrueFalse()) {
            getRestrictedGeneratedData();
        } else {
            generateData();
        }
    }

    private static int getIntBetween(int smallest, int biggest) {

        int provided;

        do {

            System.out.print("Provide number between " + smallest + " and " + biggest + ": ");

            try {
                provided = Integer.parseInt(sc.nextLine().split("\\s")[0]);
            } catch (NumberFormatException nfe) {
                provided = -1;
            }

        } while (provided < smallest || biggest < provided);

        return provided;
    }

    private static boolean getTrueFalse() {

        char choice;

        do {
            choice = sc.nextLine().toLowerCase().charAt(0);
        } while (choice != 'y' && choice != 'n');

        switch (choice) {
            case 'y':
                return true;
            case 'n':
                return false;
            default:
                System.out.println("Something went wrong");
                return false;
        }

    }

    private static void getRestrictedGeneratedData() {

        System.out.println("Provide how many processes do you want in the queue?: ");
        nOfProcesses = getIntBetween(1, 1000);

        System.out.println("Provide max time of cpu usage: ");
        maxCpuTime = getIntBetween(1, 1000);

        System.out.println("Provide how late can process arrive: ");
        maxArrivalTime = getIntBetween(1, (nOfProcesses * maxCpuTime) / 2);

        System.out.println("Provide quantity of time for RR: ");
        quantityOfTime = getIntBetween(1, maxCpuTime);
    }

    //creates a queue of 10-100 processes, each taking 1-100ms and arriving at 0-50*number of processes time
    //quantity of time is set to a number between 1 and 150
    private static void generateData() {

        ArrayList<Process> queue = new ArrayList<>();

        nOfProcesses = (int) (Math.random() * 90) + 10;
        quantityOfTime = (int) (Math.random() * 99) + 1;
        maxCpuTime = 100;
        maxArrivalTime = (nOfProcesses * 50);
    }

    private static ArrayList<Process> generate(int numberOfProcesses, int maxTimeCpuUsage, int howLate) {

        ArrayList<Process> queue = new ArrayList<>();

        for (int i = 0; i < numberOfProcesses; i++) {

            queue.add(
                    new Process(
                            (int) (Math.random() * (maxTimeCpuUsage - 1)) + 1,
                            (int) (Math.random() * (howLate - 1)) + 1)
            );

        }

        return queue;
    }

}
