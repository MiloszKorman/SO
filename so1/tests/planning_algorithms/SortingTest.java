package planning_algorithms;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import process.Process;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SortingTest {

    private ArrayList<Process> processesQueue = new ArrayList<>();

    private Process process1;
    private Process process2;
    private Process process3;
    private Process process4;

    @BeforeEach
    public void setUp() {
        processesQueue.add(process1 = new Process(12, 5));
        processesQueue.add(process2 = new Process(26, 3));
        processesQueue.add(process3 = new Process(20, 0));
        processesQueue.add(process4 = new Process(14, 8));
    }

    @Test
    public void timeOfArrivalSortTest() {
        Collections.sort(processesQueue, Process::compareByArrival);

        assertEquals(process3, processesQueue.get(0));
        assertEquals(process2, processesQueue.get(1));
        assertEquals(process1, processesQueue.get(2));
        assertEquals(process4, processesQueue.get(3));
    }

    @Test
    public void timeOfCpuUsageTest() {
        Collections.sort(processesQueue, Process::compareByRemainingCpuTime);

        assertEquals(process1, processesQueue.get(0));
        assertEquals(process4, processesQueue.get(1));
        assertEquals(process3, processesQueue.get(2));
        assertEquals(process2, processesQueue.get(3));
    }

}
