package page_replacement;

import page.Page;

import java.util.Comparator;

public class LRU extends PageReplacementAlgorithm {

    public LRU(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    protected Page nextToRemove() {
        return frames.stream()
                .min(Comparator.comparingInt(Page::getTimeOfLastUsage))
                .get();
    }
}
