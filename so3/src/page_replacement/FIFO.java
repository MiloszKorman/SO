package page_replacement;

import page.Page;

import java.util.Comparator;

public class FIFO extends PageReplacementAlgorithm {

    public FIFO(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    protected Page nextToRemove() {
        return frames.stream()
                .min(Comparator.comparingInt(Page::getTimeOfArrival))
                .get();
    }
}
