package run;

import page.Page;
import page_replacement.*;

import java.util.*;

public class Runner {

    static Scanner sc = new Scanner(System.in);
    static SplittableRandom random = new SplittableRandom();

    static int nOfFrames = random.nextInt(3, 21);
    static int nOfPages = random.nextInt(nOfFrames, 101);
    static int nOfIncomingPages = random.nextInt(1000, 10000);
    static int accuracy;

    static List<PageReplacementAlgorithm> VMManagerLessFrames = new LinkedList<>();
    static List<PageReplacementAlgorithm> VMManager = new LinkedList<>();
    static List<PageReplacementAlgorithm> VMManagerMoreFrames = new LinkedList<>();

    static List<List<PageReplacementAlgorithm>> listsOfAlgorithms = new LinkedList<>();

    public static void main(String[] args) {

        System.out.println("How accurate do you want simulation to be?: ");
        accuracy = getAnInt(1, 100);

        System.out.print("Do you want to provide restrictions?: ");
        if (sc.nextLine().toUpperCase().toCharArray()[0] == 'Y') {
            getRestrictions();
        }

        initializeReplacementAlgorithms();

        for (int i = 0; i < accuracy; i++) {
            List<Page> incomingPages = generateQueue();

            for (List<PageReplacementAlgorithm> algorithms : listsOfAlgorithms) {
                for (PageReplacementAlgorithm algorithm : algorithms) {
                    algorithm.simulate(new LinkedList<>(incomingPages));
                }
            }
        }

        System.out.println("\n" + "Frames available: " + nOfFrames);
        System.out.println("Pages requested: 1 - " + nOfPages);
        System.out.println("Incoming requests: " + nOfIncomingPages);
        System.out.println("Accuracy of the simulation: " + accuracy);

        System.out.println("\n\n");
        for (List<PageReplacementAlgorithm> algorithms : listsOfAlgorithms) {
            System.out.println("Frames: " + algorithms.get(0).getnOfFrames());
            for (PageReplacementAlgorithm algorithm : algorithms) {
                System.out.println(
                        algorithm.getClass().getSimpleName() + ": " +
                                algorithm.getAveragePageErrors() + " page errors on average"
                );
            }
            System.out.println();
        }

    }

    static void getRestrictions() {
        System.out.println("How many frames?: ");
        nOfFrames = getAnInt(3, 20);
        System.out.println("How many pages?: ");
        nOfPages = getAnInt(nOfFrames, 100);
        System.out.println("How many incoming pages?: ");
        nOfIncomingPages = getAnInt(1000, 10000);
    }

    /*static List<Page> generateQueue() {

        List<Page> incomingPages = new LinkedList<>();

        int nOfLocales = nOfIncomingPages/1000;
        int rest = nOfIncomingPages-nOfLocales;
        int perLocale = (int)Math.ceil(rest/(double)nOfLocales);

        int howManyPages = nOfIncomingPages;

        for (int i = 0; i < nOfLocales; i++) {
            int locale = random.nextInt(1, nOfPages+1);
            incomingPages.add(new Page(locale));
            howManyPages--;
            for (int j = 0; j < perLocale && 0 < howManyPages; j++) {
                int lowerBound = locale - 5;
                if (lowerBound < 1) {
                    lowerBound = 1;
                }
                int upperBound = locale + 5;
                if (nOfPages < upperBound) {
                    upperBound = nOfPages;
                }
                incomingPages.add(new Page(random.nextInt(lowerBound, upperBound+1)));
                howManyPages--;
            }
        }

        return incomingPages;
    }*/

    static List<Page> generateQueue() {
        List<Page> incomingPages = new LinkedList<>();

        for (int i = 0; i < nOfIncomingPages; i++) {
            incomingPages.add(new Page(random.nextInt(1, nOfPages+1)));
        }

        return incomingPages;
    }

    static void initializeReplacementAlgorithms() {
        VMManagerLessFrames.add(new FIFO(nOfFrames/2));
        VMManagerLessFrames.add(new RAND(nOfFrames/2));
        VMManagerLessFrames.add(new ALRU(nOfFrames/2));
        VMManagerLessFrames.add(new LRU(nOfFrames/2));
        VMManagerLessFrames.add(new OPT(nOfFrames/2));

        VMManager.add(new FIFO(nOfFrames));
        VMManager.add(new RAND(nOfFrames));
        VMManager.add(new ALRU(nOfFrames));
        VMManager.add(new LRU(nOfFrames));
        VMManager.add(new OPT(nOfFrames));

        VMManagerMoreFrames.add(new FIFO(nOfFrames*3/2));
        VMManagerMoreFrames.add(new RAND(nOfFrames*3/2));
        VMManagerMoreFrames.add(new ALRU(nOfFrames*3/2));
        VMManagerMoreFrames.add(new LRU(nOfFrames*3/2));
        VMManagerMoreFrames.add(new OPT(nOfFrames*3/2));

        listsOfAlgorithms.add(VMManagerLessFrames);
        listsOfAlgorithms.add(VMManager);
        listsOfAlgorithms.add(VMManagerMoreFrames);
    }

    private static int getAnInt(int min, int max) {

        int toReturn = max + 1;

        while (toReturn < min || max < toReturn) {

            System.out.print("Provide an int between " + min + " and " + max + ": ");

            try {
                toReturn = Integer.parseInt(sc.nextLine().split("\\s+")[0]);
            } catch (NumberFormatException nfe) {}

        }

        System.out.println();

        return toReturn;
    }

}
