package page;

public class Page {

    private int number;
    private int timeOfArrival = Integer.MAX_VALUE;
    private int timeOfLastUsage = Integer.MAX_VALUE;
    private boolean secondChance = true;

    public Page(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getTimeOfArrival() {
        return timeOfArrival;
    }

    public void setTimeOfArrival(int timeOfArrival) {
        this.timeOfArrival = timeOfArrival;
        this.timeOfLastUsage = timeOfArrival;
    }

    public int getTimeOfLastUsage() {
        return timeOfLastUsage;
    }

    public void setTimeOfLastUsage(int timeOfLastUsage) {
        this.timeOfLastUsage = timeOfLastUsage;
    }

    public boolean isSecondChance() {
        return secondChance;
    }

    public void setSecondChance(boolean secondChance) {
        this.secondChance = secondChance;
    }

    @Override
    public boolean equals(Object obj) {
        return (this.number == ((Page)obj).number);
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(number);
    }

}