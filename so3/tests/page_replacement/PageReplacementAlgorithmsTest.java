package page_replacement;

import org.junit.jupiter.api.Test;
import page.Page;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SplittableRandom;

import static org.junit.jupiter.api.Assertions.*;

class PageReplacementAlgorithmsTest {

    @Test
    void FIFOTest() {
        assertEquals(15, (new FIFO(3)).simulate(bookTestQueue()));
    }

    @Test
    void OPTTest() {
        assertEquals(9, (new OPT(3)).simulate(bookTestQueue()));
    }

    @Test
    void LRUTest() {
        assertEquals(12, (new LRU(3)).simulate(bookTestQueue()));
    }

    @Test
    void ALRUTest() {
        assertEquals(11, (new ALRU(3)).simulate(bookTestQueue()));
    }

    private List<Page> bookTestQueue() {
        List<Page> testingQueue = Arrays.asList(
                new Page(7),
                new Page(0),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(3),
                new Page(0),
                new Page(4),
                new Page(2),
                new Page(3),
                new Page(0),
                new Page(3),
                new Page(2),
                new Page(1),
                new Page(2),
                new Page(0),
                new Page(1),
                new Page(7),
                new Page(0),
                new Page(1)
        );

        return new ArrayList<>(testingQueue);
    }

}