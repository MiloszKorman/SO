package run;

import operations.Process;
import operations.Processor;
import processor_allocation.ProcessorAllocationAlgorithm;
import processor_allocation.XCaring;
import processor_allocation.XFirst;
import processor_allocation.XLast;
import processor_allocation.results.Stats;

import java.util.*;
import java.util.stream.Collectors;

public class Runner {

    static SplittableRandom random = new SplittableRandom();
    static Scanner sc = new Scanner(System.in);

    static int nOfProcessors = random.nextInt(40, 101);
    static int maxNOfProcessesPerProcessor = random.nextInt(100, 200);
    static int maxNOfInquiries = random.nextInt(1, (maxNOfProcessesPerProcessor + 1)/2);
    static int lowerBoundary = random.nextInt(1, 50);
    static int upperBoundary = random.nextInt(50, 101);
    static int accuracy = 1;

    private static List<ProcessorAllocationAlgorithm> algorithms = new LinkedList<>();

    public static void main(String[] args) {

        System.out.print("Do you want to provide restrictions?: ");
        if (sc.nextLine().toUpperCase().toCharArray()[0] == 'Y') {
            getRestrictions();
        }

        initializeAlgorithms();

        for (int i = 0; i < accuracy; i++) {
            Set<Processor> processors = generateQueue();

            for (ProcessorAllocationAlgorithm algorithm : algorithms) {
                algorithm.simulate(processors.stream().map(Processor::clone).collect(Collectors.toSet()));
            }
        }

        System.out.println(
                "\n\nProcessors: " + nOfProcessors +
                        "\nMax number of processes per processor: " + maxNOfProcessesPerProcessor +
                        "\nMax number of inquiries: " + maxNOfInquiries +
                        "\nLower boundary: " + lowerBoundary +
                        "\nUpper boundary: " + upperBoundary +
                        "\n\n"
        );

        for (ProcessorAllocationAlgorithm algorithm : algorithms) {
            Stats algorithmsStats = algorithm.getStats();
            System.out.println(algorithm.getClass().getSimpleName());
            System.out.println("Average Processors Usage: " + algorithmsStats.getAvgProcessorsUsage());
            System.out.println("Average Deviation: " + algorithmsStats.getAvgDeviation());
            System.out.println("Number of inquiries: " + algorithmsStats.getNumberOfInquiries());
            System.out.println("Number of migrations: " + algorithmsStats.getNumberOfMigrations());
            System.out.println();
        }

    }

    private static void getRestrictions() {
        System.out.println("How many processors do you want to be in the simulation?: ");
        nOfProcessors = getAnInt(50, 100);
        System.out.println("What should be max number of processes per processor?: ");
        maxNOfProcessesPerProcessor = getAnInt(500, 10000);
        System.out.println("What should be max amount on inquiries?: ");
        maxNOfInquiries = getAnInt(1, maxNOfProcessesPerProcessor);
        System.out.println("What should be lower boundary of processors load?: ");
        lowerBoundary = getAnInt(1, 49);
        System.out.println("What should be upper boundary of processors load?: ");
        upperBoundary = getAnInt(50, 100);
    }

    private static void initializeAlgorithms() {
        algorithms.add(new XLast(maxNOfInquiries, upperBoundary, lowerBoundary));
        algorithms.add(new XFirst(maxNOfInquiries, upperBoundary, lowerBoundary));
        algorithms.add(new XCaring(maxNOfInquiries, upperBoundary, lowerBoundary));
    }

    private static Set<Processor> generateQueue() {
        Set<Processor> processors = new HashSet<>();

        for (int i = 0; i < nOfProcessors; i++) {

            int nOfProcesses = random.nextInt(100, maxNOfProcessesPerProcessor+1);
            List<Process> processorsProcesses = new ArrayList<>(nOfProcesses);

            for (int j = 0; j < nOfProcesses; j++) {
                processorsProcesses.add(new Process(
                        random.nextInt(0, maxNOfProcessesPerProcessor/10),
                        random.nextInt(1, 11),
                        random.nextInt(1, 11)
                ));
            }

            processors.add(new Processor(processorsProcesses));

        }

        return processors;
    }

    private static int getAnInt(int min, int max) {

        int toReturn = max + 1;

        while (toReturn < min || max < toReturn) {

            System.out.print("Provide an int between " + min + " and " + max + ": ");

            try {
                toReturn = Integer.parseInt(sc.nextLine().split("\\s+")[0]);
            } catch (NumberFormatException nfe) {
            }

        }

        System.out.println();

        return toReturn;
    }

}
