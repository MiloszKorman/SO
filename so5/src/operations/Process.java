package operations;

public class Process {

    private int timeOfArrival;
    private int cpuUsageLevel;
    private int timeOfCpuUsage;

    private Processor origin = null;

    public Process(int timeOfArrival, int cpuUsageLevel, int timeOfCpuUsage) {
        this.timeOfArrival = timeOfArrival;
        this.cpuUsageLevel = cpuUsageLevel;
        this.timeOfCpuUsage = timeOfCpuUsage;
    }

    public int getTimeOfArrival() {
        return timeOfArrival;
    }

    public int getCpuUsageLevel() {
        return cpuUsageLevel;
    }

    public int getTimeOfCpuUsage() {
        return timeOfCpuUsage;
    }

    public Processor getOrigin() {
        return origin;
    }

    public void setOrigin(Processor origin) {
        this.origin = origin;
    }

    public void iteration() {
        if (0 < timeOfCpuUsage) {
            timeOfCpuUsage--;
        }
    }

    public Process clone() {
        return new Process(timeOfArrival, cpuUsageLevel, timeOfCpuUsage);
    }

}
