package operations;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class Processor {

    private int currentUsage = 0;
    private List<Process> waitingProcesses;
    private List<Process> workingProcesses = new ArrayList<>();

    public Processor(List<Process> waitingProcesses) {
        this.waitingProcesses = waitingProcesses;
        waitingProcesses.forEach(process -> process.setOrigin(this));
    }

    public int getCurrentUsage() {
        return currentUsage;
    }

    public void setCurrentUsage(int currentUsage) {
        this.currentUsage = currentUsage;
    }

    public void executeProcess(Process processToExecute) {
        if (processToExecute.getCpuUsageLevel() <= 100 - currentUsage) {
            workingProcesses.add(processToExecute);
            currentUsage += processToExecute.getCpuUsageLevel();
        } else {
            waitingProcesses.add(processToExecute);
        }
    }

    public Process giveBackProcess() {
        Process processToReturn = workingProcesses.stream().findAny().orElse(null);
        if (processToReturn != null) {
            currentUsage -= processToReturn.getCpuUsageLevel();
        }
        return processToReturn;
    }

    public List<Process> getWaitingProcesses() {
        return waitingProcesses;
    }

    public List<Process> getWorkingProcesses() {
        return workingProcesses;
    }

    public void iteration() {
        for (Process process : workingProcesses) {
            process.iteration();
        }
        workingProcesses.removeAll( //remove finished
                workingProcesses.stream()
                        .filter(process -> process.getTimeOfCpuUsage() == 0)
                        .collect(Collectors.toSet())
        );
        currentUsage = workingProcesses.stream() //calc current cpu usage
                .map(Process::getCpuUsageLevel)
                .reduce(0, (acc, cpuUsage) -> acc + cpuUsage);
    }

    public Processor clone() {
        return new Processor(waitingProcesses.stream().map(Process::clone).collect(Collectors.toList()));
    }

}
