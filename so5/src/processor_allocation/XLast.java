package processor_allocation;

import operations.Process;
import operations.Processor;

import java.util.List;
import java.util.Set;

public class XLast extends ProcessorAllocationAlgorithm {

    public XLast(int MAX_N_OF_INQUIRIES, int UPPER_BOUND, int LOWER_BOUND) {
        super(MAX_N_OF_INQUIRIES, UPPER_BOUND, LOWER_BOUND);
    }

    @Override
    protected void allocateProcesses(Set<Processor> workingProcessors, List<Process> toRemove,
                                     List<Processor> possibleExecutors, Process process) {

        Processor foreignProcessor = pickForeignProcessor(possibleExecutors, process);
        if (foreignProcessor == null) {
            if (process.getOrigin().getCurrentUsage() + process.getCpuUsageLevel() <= 100) {
                process.getOrigin().executeProcess(process);
                toRemove.add(process);
            }
        } else {
            executeOnForeignProcessor(foreignProcessor, process, toRemove);
        }

    }

}
