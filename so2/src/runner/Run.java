package runner;

import orders.Order;
import planners.*;
import planners.moving.CSCAN;
import planners.moving.SCAN;
import priority.handling.PriorityHandlingMethod;

import java.util.ArrayList;
import java.util.Scanner;

public class Run {

    private static Scanner sc = new Scanner(System.in);

    static final int STARTING_SECTOR = (int) (Math.random() * PlanningAlgorithm.getMaxSector()) + 1;

    private static int queueSize;
    private static int accuracy;
    private static int howLate;
    private static int nOfDeadliners;
    private static int maxDeadline;
    private static int minDeadline;
    private static ArrayList<ArrayList<PlanningAlgorithm>> algorithms = new ArrayList<>();
    private static ArrayList<PlanningAlgorithm> regularAlgorithms = new ArrayList<>();
    private static ArrayList<PlanningAlgorithm> edfAlgorithms = new ArrayList<>();
    private static ArrayList<PlanningAlgorithm> fdscanAlgorithms = new ArrayList<>();
    private static ArrayList<Order> ordersQueue = new ArrayList<>();

    public static void main(String[] args) {

        regularAlgorithms.add(new FCFS(STARTING_SECTOR));
        regularAlgorithms.add(new SSTF(STARTING_SECTOR));
        regularAlgorithms.add(new SCAN(STARTING_SECTOR));
        regularAlgorithms.add(new CSCAN(STARTING_SECTOR));

        edfAlgorithms.add(new FCFS(STARTING_SECTOR, PriorityHandlingMethod.EDF));
        edfAlgorithms.add(new SSTF(STARTING_SECTOR, PriorityHandlingMethod.EDF));
        edfAlgorithms.add(new SCAN(STARTING_SECTOR, PriorityHandlingMethod.EDF));
        edfAlgorithms.add(new CSCAN(STARTING_SECTOR, PriorityHandlingMethod.EDF));

        fdscanAlgorithms.add(new FCFS(STARTING_SECTOR, PriorityHandlingMethod.FDSCAN));
        fdscanAlgorithms.add(new SSTF(STARTING_SECTOR, PriorityHandlingMethod.FDSCAN));
        fdscanAlgorithms.add(new SCAN(STARTING_SECTOR, PriorityHandlingMethod.FDSCAN));
        fdscanAlgorithms.add(new CSCAN(STARTING_SECTOR, PriorityHandlingMethod.FDSCAN));

        algorithms.add(regularAlgorithms);
        algorithms.add(edfAlgorithms);
        algorithms.add(fdscanAlgorithms);

        printAssumptions();
        System.out.println();

        getRestrictions();

        System.out.println("Do you want to print steps?");
        if (getYN()) {
            printSteps();
        } else {
            noPrintSteps();
        }
        System.out.println();
        System.out.println();

        printResults();

        sc.close();
    }

    private static void printAssumptions() {
        System.out.println(
                "Disks size is 512 kB\n" +
                        "Sectors size is 512 B\n" +
                        "Meaning there are 1000 sectors\n" +
                        "In non-real time algorithms, deadline orders are erased before simulation\n" +
                        "Starting sector is: " + STARTING_SECTOR
        );
    }


    private static void printSteps() {

        for (int i = 1; i <= accuracy; i++) {

            generateOrders();

            System.out.println("Iteration: " + i);
            for (ArrayList<PlanningAlgorithm> algorithmsList : algorithms) {
                for (PlanningAlgorithm algorithm : algorithmsList) {
                    System.out.println(
                            algorithm +
                                    ", total head travel distance: " +
                                    algorithm.simulate(new ArrayList<>(ordersQueue)) +
                                    ", deadlines not finished in time: " +
                                    algorithm.howManyPastDeadline()
                    );
                }
                System.out.println();
            }
            System.out.println();
            System.out.println();
        }

    }

    private static void noPrintSteps() {

        for (int i = 1; i <= accuracy; i++) {

            generateOrders();

            for (ArrayList<PlanningAlgorithm> algorithmsList : algorithms) {
                for (PlanningAlgorithm algorithm : algorithmsList) {
                    algorithm.simulate(new ArrayList<>(ordersQueue));
                }
            }
        }
    }

    private static void printResults() {

        System.out.println(queueSize + " orders coming before " + howLate);
        System.out.println("In which " + nOfDeadliners + " with deadline between: " + minDeadline + " and " + maxDeadline);

        for (ArrayList<PlanningAlgorithm> algorithmsList : algorithms) {
            for (PlanningAlgorithm algorithm : algorithmsList) {
                System.out.println(
                        algorithm +
                                ", average head travel distance: " +
                                algorithm.getAverageHeadMoves() +
                                ", average number od deadlines not finished in time: " +
                                algorithm.getAverageMissedDeadlines()
                );
            }
            System.out.println();
        }

    }

    private static void getRestrictions() {

        System.out.println("How accurate do you want the simulation to be?: ");
        accuracy = getAnInt(1, 100);

        System.out.println("Do you want to provide Restrictions?: ");

        if (getYN()) {
            System.out.println("What should size of the queue be?: ");
            queueSize = getAnInt(1, 1000);
            System.out.println("How late can an order arrive?: ");
            howLate = getAnInt(0, (queueSize * PlanningAlgorithm.getMaxSector() / 4));
            System.out.println("How many of the orders should have deadlines?: ");
            nOfDeadliners = getAnInt(1, queueSize - 1);
            System.out.println("What is the minimal deadline?: ");
            minDeadline = getAnInt(1, 999);
            System.out.println("What is the maximal deadline?: ");
            maxDeadline = getAnInt(minDeadline + 1, 2000);
        } else {
            queueSize = (int) (Math.random() * 900) + 101;
            howLate = (int) (Math.random() * (queueSize * PlanningAlgorithm.getMaxSector() / 4));
            nOfDeadliners = (int) (Math.random() * queueSize);
            maxDeadline = 1000;
            minDeadline = 100;
        }

    }

    private static int getAnInt(int min, int max) {

        int toReturn = max + 1;

        while (toReturn < min || max < toReturn) {

            System.out.print("Provide an int between " + min + " and " + max + ": ");

            try {
                toReturn = Integer.parseInt(sc.nextLine().split("\\s+")[0]);
            } catch (NumberFormatException nfe) {}

        }

        System.out.println();

        return toReturn;
    }

    private static boolean getYN() {
        char c = 's';
        while (c != 'Y' && c != 'N') {
            System.out.print("(Y/N): ");
            c = Character.toUpperCase(sc.nextLine().charAt(0));
        }

        return (c == 'Y');
    }


    private static void generateOrders() {

        ordersQueue.clear();

        for (int i = 0; i < queueSize - nOfDeadliners; i++) {
            ordersQueue.add(new Order(
                    (int) (Math.random() * 1000) + 1,
                    (int) (Math.random() * howLate) + 1
            ));
        }

        for (int i = 0; i < nOfDeadliners; i++) {
            ordersQueue.add(new Order(
                    (int) (Math.random() * 1000) + 1,
                    (int) (Math.random() * (howLate + 1)),
                    (int) (Math.random() * (maxDeadline - minDeadline + 1)) + minDeadline
            ));
        }

    }

}
