package exception;

public class ImpossibleToCarryOutException extends RuntimeException {
    public ImpossibleToCarryOutException() {
        super("With provided data it's impossible to carry out simulation");
    }
}
