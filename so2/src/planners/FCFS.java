package planners;

import orders.Order;
import priority.handling.PriorityHandlingMethod;

public class FCFS extends PlanningAlgorithm {

    public FCFS(int startingSector) {
        super(startingSector);
    }

    public FCFS(int startingSector, PriorityHandlingMethod priorityHandling) {
        super(startingSector, priorityHandling);
    }

    @Override
    protected Order getNextRegularOrder() {
        if (availableOrders.isEmpty()) {
            return null;
        } else {
            return availableOrders.get(0);
        }
    }

}
