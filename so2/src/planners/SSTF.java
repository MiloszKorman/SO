package planners;

import orders.Order;
import priority.handling.PriorityHandlingMethod;

public class SSTF extends PlanningAlgorithm {

    public SSTF(int startingSector) {
        super(startingSector);
    }

    public SSTF(int startingSector, PriorityHandlingMethod priorityHandling) {
        super(startingSector, priorityHandling);
    }

    @Override
    protected Order getNextRegularOrder() {
        return findNearest(availableOrders);
    }

}
