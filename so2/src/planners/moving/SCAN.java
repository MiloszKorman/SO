package planners.moving;

import orders.Order;
import directions.Direction;
import priority.handling.PriorityHandlingMethod;

import java.util.ArrayList;

public class SCAN extends MovingPlanningAlgorithm {

    public SCAN(int startingSector) {
        super(startingSector);
    }

    public SCAN(int startingSector, PriorityHandlingMethod priorityHandling) {
        super(startingSector, priorityHandling);
    }

    @Override
    public int simulate(ArrayList<Order> ordersQueue) {
        return super.simulate(ordersQueue);
    }

    @Override
    protected Order availableOrdersInWrongDirection() {
        int timeComing;
        if (currentDirection == Direction.RIGHT) {
            timeComing = isOrderComingWithinThisCycle(MAX_SECTOR);
            if (timeComing == -1) {
                currentDirection = Direction.LEFT;
            }
            return new Order(MAX_SECTOR, 0);
        } else {
            timeComing = isOrderComingWithinThisCycle(1);
            if (timeComing == -1) {
                currentDirection = Direction.RIGHT;
            }
            return new Order(1, 0);
        }
    }

}
