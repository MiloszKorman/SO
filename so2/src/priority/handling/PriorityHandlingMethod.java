package priority.handling;

public enum PriorityHandlingMethod {
    EDF, FDSCAN, NONE
}
