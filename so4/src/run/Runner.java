package run;

import frame_allocation.FrameAllocationAlgorithm;
import frame_allocation.global.PageFaultFrequency;
import frame_allocation.global.WorkingSet;
import frame_allocation.local.Equal;
import frame_allocation.local.Proportional;
import page.Page;
import process.Process;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.SplittableRandom;
import java.util.stream.Collectors;

public class Runner {

    static Scanner sc = new Scanner(System.in);
    static SplittableRandom random = new SplittableRandom();


    static int nOfProcesses = random.nextInt(10, 101);
    static int nOfFrames = random.nextInt(nOfProcesses*10, 1001);
    static int maxPageRequests = random.nextInt(10, 10001);
    static int accuracy;

    static List<FrameAllocationAlgorithm> listOfAlgorithms = new LinkedList<>();

    public static void main(String[] args) {

        System.out.println("How accurate do you want simulation to be?: ");
        accuracy = getAnInt(1, 100);

        System.out.print("Do you want to provide restrictions?: ");
        if (sc.nextLine().toUpperCase().toCharArray()[0] == 'Y') {
            getRestrictions();
        }

        initializeFrameAllocationAlgorithms();

        for (int i = 0; i < accuracy; i++) {
            List<Process> processes = generateQueue();

            for (FrameAllocationAlgorithm algorithm : listOfAlgorithms) {
                algorithm.simulate(processes.stream().map(Process::cloneProcess).collect(Collectors.toList()));
            }
        }

        System.out.println(
                "\nNumber of processes: " + nOfProcesses + "\n" +
                        "Number of global frames: " + nOfFrames + "\n" +
                        "Max Page Requests per Process: " + maxPageRequests + "\n"
        );

        for (FrameAllocationAlgorithm algorithm : listOfAlgorithms) {
            System.out.println(
                    algorithm.getClass().getSimpleName() +
                            " average number of global page errors : " +
                            algorithm.getAvgNumOfGlobalPageErrors() +
                            " average number of page errors per process: " +
                            algorithm.getAvgNumOfPageErrorsPerProcess()
            );
        }

    }

    private static void getRestrictions() {
        System.out.println("How many processes?: ");
        nOfProcesses = getAnInt(10, 100);
        System.out.println("How many frames?: ");
        nOfFrames = getAnInt(nOfProcesses*10, 1000);
        System.out.println("Max page requests per process?: ");
        maxPageRequests = getAnInt(10, 10000);
    }

    private static int getAnInt(int min, int max) {

        int toReturn = max + 1;

        while (toReturn < min || max < toReturn) {

            System.out.print("Provide an int between " + min + " and " + max + ": ");

            try {
                toReturn = Integer.parseInt(sc.nextLine().split("\\s+")[0]);
            } catch (NumberFormatException nfe) {}

        }

        System.out.println();

        return toReturn;
    }

    private static void initializeFrameAllocationAlgorithms() {
        listOfAlgorithms.add(new Equal(nOfFrames));
        listOfAlgorithms.add(new Proportional(nOfFrames));
        listOfAlgorithms.add(new WorkingSet(nOfFrames));
        listOfAlgorithms.add(new PageFaultFrequency(nOfFrames));
    }

    private static List<Process> generateQueue() {

        List<Process> processes = new LinkedList<>();
        List<Page> processesPages = new LinkedList<>();

        for (int i = 0; i < nOfProcesses; i++) {

            int nOfIncomingPages = random.nextInt(10, maxPageRequests+1);
            int nOfLocales = nOfIncomingPages/10;
            int rest = nOfIncomingPages - nOfLocales;
            int perLocale = (int)Math.ceil(rest/(double)nOfLocales);

            for (int j = 0; j < nOfLocales; j++) {

                int locale = random.nextInt(1, 1000);
                processesPages.add(new Page(locale));
                nOfIncomingPages--;
                for (int k = 0; k < perLocale && 0 < nOfIncomingPages; k++) {
                    int lowerBound = locale - 10;
                    if (lowerBound < 1) {
                        lowerBound = 1;
                    }
                    int upperBound = locale + 10;
                    if (1000 < upperBound) {
                        upperBound = 1000;
                    }
                    processesPages.add(new Page(random.nextInt(lowerBound, upperBound)));
                    nOfIncomingPages--;
                }

            }

            Process newProcess = new Process(processesPages.size()/10);
            newProcess.setProcessesIncomingPages(new LinkedList<>(processesPages));
            processes.add(newProcess);
            processesPages.clear();
        }

        return processes;
    }

}
