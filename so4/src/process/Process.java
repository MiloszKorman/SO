package process;

import page.Page;

import java.util.LinkedList;
import java.util.List;

public class Process implements Comparable {

    int size;
    int minFrames;
    List<Page> processesIncomingPages;

    int nOfSimulations = 0;
    int pageErrorsSum = 0;

    int allocatedFrames;

    public Process(int minFrames) {
        this.minFrames = minFrames;
        this.allocatedFrames = minFrames;
    }

    public Process(List<Page> processesIncomingPages, int minFrames) {
        this(minFrames);
        setProcessesIncomingPages(processesIncomingPages);
    }

    public void setProcessesIncomingPages(List<Page> processesIncomingPages) {
        for (Page page : processesIncomingPages) {
            page.setOwner(this);
        }
        this.processesIncomingPages = processesIncomingPages;
        size = processesIncomingPages.size();
    }

    public int getSize() {
        return size;
    }

    public List<Page> getProcessesIncomingPages() {
        return processesIncomingPages;
    }

    public void simulationFinished(int pageErrors) {
        pageErrorsSum += pageErrors;
        nOfSimulations++;
    }

    public int getMin() {
        return minFrames;
    }

    public int getAllocatedFrames() {
        return allocatedFrames;
    }

    public void setAllocatedFrames(int allocatedFrames) {
        this.allocatedFrames = allocatedFrames;
    }

    public Process cloneProcess() {
        Process processToReturn = new Process(minFrames);
        processToReturn.setProcessesIncomingPages(new LinkedList<>(processesIncomingPages));
        return processToReturn;
    }

    @Override
    public int compareTo(Object o) {
        return Integer.compare(this.size, ((Process)o).getSize());
    }
}
