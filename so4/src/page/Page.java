package page;

import process.Process;

public class Page {

    private int number;
    private int timeOfArrival = Integer.MAX_VALUE;
    private int timeOfLastUsage = Integer.MAX_VALUE;
    private boolean secondChance = true;

    Process owner;

    public Page(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getTimeOfArrival() {
        return timeOfArrival;
    }

    public void setTimeOfArrival(int timeOfArrival) {
        this.timeOfArrival = timeOfArrival;
        this.timeOfLastUsage = timeOfArrival;
    }

    public int getTimeOfLastUsage() {
        return timeOfLastUsage;
    }

    public void setTimeOfLastUsage(int timeOfLastUsage) {
        this.timeOfLastUsage = timeOfLastUsage;
    }

    public boolean isSecondChance() {
        return secondChance;
    }

    public void setSecondChance(boolean secondChance) {
        this.secondChance = secondChance;
    }

    public Process getOwner() {
        return owner;
    }

    public void setOwner(Process owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object obj) {
        return (this.number == ((Page)obj).number);
    }

}
