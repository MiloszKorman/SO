package page_replacement;

import page.Page;

import java.util.ArrayList;
import java.util.List;

public abstract class PageReplacementAlgorithm {

    protected List<Page> frames = new ArrayList<>();
    protected List<Page> listOfPages;
    protected int pageErrors;
    protected int clock;

    public int simulate(List<Page> incomingPages, int allocatedFrames) {
        pageErrors = 0;
        clock = 0;
        this.listOfPages = incomingPages;
        frames = new ArrayList<>(allocatedFrames);

        while (!incomingPages.isEmpty()) {
            simulateIteration(incomingPages.remove(0), allocatedFrames);
        }

        return pageErrors;
    }

    public void simulateIteration(Page currentPage, int allocatedFrames) {

        if (frames.contains(currentPage)) {
            frames.get(frames.indexOf(currentPage)).setTimeOfLastUsage(clock);
        }

        while (allocatedFrames < frames.size()) {
            frames.remove(nextToRemove());
        }

        if (!frames.contains(currentPage)) {
            currentPage.setTimeOfArrival(clock);

            if (frames.size() == allocatedFrames) {
                frames.remove(nextToRemove());
            }

            frames.add(currentPage);

            pageErrors++;
        }

        clock++;
    }

    protected abstract Page nextToRemove();

    public int getPageErrors() {
        return pageErrors;
    }
}
