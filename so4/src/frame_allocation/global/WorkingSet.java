package frame_allocation.global;

import frame_allocation.FrameAllocationAlgorithm;
import page.Page;
import page_replacement.LRU;
import process.Process;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class WorkingSet extends FrameAllocationAlgorithm {

    private static int delta = 10;
    private HashMap<Process, Queue> history = new HashMap<>();

    public static void setDelta(int delta) {
        WorkingSet.delta = delta;
    }

    public WorkingSet(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    public int calcFramesForProcess(Process process) {
        int framesToAllocate = history.get(process).uniqueSize();

        if (framesToAllocate <= availableFrames) {
            history.get(process).add(process.getProcessesIncomingPages().get(0));
        }

        return (framesToAllocate < process.getMin()) ? process.getMin() : framesToAllocate;
    }

    @Override
    protected void initializeMaps(List<Process> processes) {
        for (Process process : processes) {
            workingProcesses.put(process, new LRU());
            history.put(process, new Queue());
        }
    }

    public static class Queue {

        List<Page> processesHistory = new LinkedList<>();

        public void add(Page page) {
            if (processesHistory.size() == delta) {
                processesHistory.remove(0);
            }
            processesHistory.add(page);
        }

        public int uniqueSize() {
            return (int)processesHistory.stream().map(Page::getNumber).distinct().count();
        }

    }

}
