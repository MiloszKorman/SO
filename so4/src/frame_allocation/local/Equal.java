package frame_allocation.local;

import frame_allocation.FrameAllocationAlgorithm;
import process.Process;

public class Equal extends FrameAllocationAlgorithm {

    public Equal(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    public int calcFramesForProcess(Process process) {

        int framesToAllocate = nOfFrames / workingProcesses.size();

        if (availableFrames < framesToAllocate) {
            framesToAllocate = availableFrames;
        }

        return (framesToAllocate < process.getMin()) ? process.getMin() : framesToAllocate;
    }

}
