package frame_allocation.local;

import frame_allocation.FrameAllocationAlgorithm;
import process.Process;

public class Proportional extends FrameAllocationAlgorithm {

    public Proportional(int nOfFrames) {
        super(nOfFrames);
    }

    @Override
    public int calcFramesForProcess(Process process) {
        int sizeSum = workingProcesses.keySet().stream()
                .map(processToSize -> processToSize.getProcessesIncomingPages().size())
                .reduce(0, (acc, processesSize) -> acc + processesSize);

        int framesToAllocate = nOfFrames * process.getProcessesIncomingPages().size() / sizeSum;

        if (availableFrames < framesToAllocate) {
            framesToAllocate = availableFrames;
        }

        return (framesToAllocate < process.getMin()) ? process.getMin() : framesToAllocate;
    }

}
