package process;

import org.junit.jupiter.api.Test;
import page.Page;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ProcessTest {

    @Test
    void cloneTest() {
        Process process = new Process(15);
        process.setProcessesIncomingPages(Arrays.asList(
                new Page(3),
                new Page(2),
                new Page(1)
        ));
        assertEquals(3, process.cloneProcess().size);
    }
}